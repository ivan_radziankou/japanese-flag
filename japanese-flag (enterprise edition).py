from math import sqrt
from argparse import ArgumentError, ArgumentParser


class GeneralFlag:
    border = '#'

    def horizontal_line(self, len):
        return self.border*len

    def blank_line(self, len):
        return '%s%s%s' % (self.border, ' '*(len - 2), self.border)


class JapaneseFlag(GeneralFlag):
    circle_border = '*'
    circle_fill = 'o'

    def validate_size(self, n):
        if n % 2:
            # we need to use Parser to raise ArgumentError properly
            argument_N = ArgumentParser().add_argument('N')
            raise ArgumentError(argument_N, message='N should be even')

    def __init__(self, n):
        self.validate_size(n)
        self.size = n
        self.radius = n // 2
        self.canvas = []
        self.canvas_width = 3*n+2

    def segment_width(self, i):
        # when the circle is big we need more accurate formula
        return i*2 if self.radius < 5 else \
            round(sqrt(self.radius**2 - (i-self.radius)**2))*2

    def draw(self):
        self.canvas.append(self.horizontal_line(canvas_width))
        for i in range(self.radius):
            self.canvas.append(self.blank_line(canvas_width))
        # align element by center
        pattern = "%s{:^%d}%s" % (self.border, n*3, self.border)
        for i in range(self.radius):
            circle_width = self.segment_width(i)
            # generate one circle line and insert into the pattern with borders
            segment = '%s%s%s' % \
                (self.circle_border,
                    self.circle_fill*circle_width,
                    self.circle_border)
            self.canvas.append(pattern.format(segment))
        # make a mirror image for the bottom half
        return '\n'.join(self.canvas+self.canvas[::-1])


def flag(n):
    # create class item and call the method
    flag = JapaneseFlag(n)
    return flag.draw()


argument = input("Enter N: ")
try:
    # get the number from the keyboard and print the flag
    n = int(argument)
    print(flag(n))
except ValueError:
    print("N should be valid integer")
except ArgumentError as arg_error:
    print(arg_error.message)
