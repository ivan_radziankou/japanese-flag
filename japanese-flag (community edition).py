from math import sqrt
from argparse import ArgumentError, ArgumentParser


def flag(n):
    if n % 2:
        argument_N = ArgumentParser().add_argument('N')
        raise ArgumentError(argument_N, message='N should be even')
    flag = []
    flag.append('#' * (3*n+2))
    radius = n // 2
    for i in range(radius):
        flag.append('#%s#' % (' '*3*n))

    pattern = "#{:^%d}#" % (n*3)
    for i in (*range(radius), *range(radius-1, -1, -1)):
        width = i*2 if n < 10 else round(sqrt(radius**2 - (i-radius)**2))*2
        flag.append(pattern.format('*%s*' % ('o'*width)))

    for i in range(radius):
        flag.append('#%s#' % (' '*3*n))
    flag.append('#' * (3*n+2))
    return '\n'.join(flag)


argument = input("Enter N: ")
try:
    n = int(argument)
    print(flag(n))
except ValueError:
    print("N should be valid integer")
except ArgumentError as arg_error:
    print(arg_error.message)
