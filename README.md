# Japanese flag

Implement a function to draw the japanese flag with an ASCII art.

There are two versions. 

## Community edition
Simple implementation made briefly as prototype

## Enterprise edition
Implementation with classes and comments

Both editions work the same. Program gets N from the console and print result to the screen.